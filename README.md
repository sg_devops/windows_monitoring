# Windows Monitoring using CloudWatch

### Monitoring using powershell script
* Using windows task schedular we can configure powershell scripts to run at particular intervals.

* Also make sure powershell got requried permissions to execute scripts

* Powershell scripts are at >powershell folder

### Monitoring using ec2config utility
* ec2config can be used to send Windows Performance Monitor data to CloudWatch

* Sample configuration is located at >ec2config folder
