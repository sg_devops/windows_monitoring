#
# Script to monitor free disk percentage and post to cloud watch
#

function get-endpoint {
	$region = "us-west-2"
	return "https://monitoring." + $region + ".amazonaws.com/"
}

#AWS credentials
$secretAccessKeyID=""
$secretKeyID=""

#Import the AWS SDK assembly
Add-Type -Path "C:\Program Files (x86)\AWS SDK for .NET\bin\Net45\AWSSDK.CloudWatch.dll"

#Get Instance ID
$instanceId = ""

#Create dimensions
$dimensions = New-Object System.Collections.Generic.List``1[Amazon.CloudWatch.Model.Dimension]

$dimension = New-Object -TypeName Amazon.CloudWatch.Model.Dimension
$dimension.Name = "InstanceID"
$dimension.Value = $instanceId

$dimensions.Add($dimension)

#Get Free Space
$freeSpace=Get-WmiObject -Class Win32_LogicalDisk | Select-Object -Property DeviceID, @{Name='FreeSpaceGB';Expression={$_.FreeSpace/1GB}}

#Create metrics
$metrics = New-Object System.Collections.Generic.List``1[Amazon.CloudWatch.Model.MetricDatum]

#Iterate and add to metrics
Foreach ($item in $freeSpace)
{
$metric = New-Object -TypeName Amazon.CloudWatch.Model.MetricDatum

$metric.MetricName = $item.DeviceID + " free_disk_space"
$metric.Value = $item.FreeSpaceGB
$metric.Unit = "Gigabytes"
$metric.Dimensions = $dimensions

$metrics.Add($metric)
}


$cwconfig = New-Object Amazon.CloudWatch.AmazonCloudWatchConfig
$cwconfig.serviceURL = get-endpoint

#Create ACW Client
$client= New-Object Amazon.Cloudwatch.AmazonCloudWatchClient($secretKeyID, $secretAccessKeyID, $cwconfig)


#Create request
$request = New-Object -TypeName Amazon.CloudWatch.Model.PutMetricDataRequest
$request.NameSpace = "Windows/Diskspace"
$request.MetricData = $metrics

#Perform the request
$response=$client.PutMetricData($request)
